import java.io.*;
class ThreadCreation1 implements Runnable {	
    public void run()
	{
		System.out.println("Inside run method");
	}
	public static void main(String args[])
	{
		// create an object of Runnable target
		ThreadCreation obj = new ThreadCreation();

		// pass the runnable reference to Thread
		Thread t = new Thread(obj, "Override");

		// start the thread
		t.start();

		// get the name of the thread
		System.out.println(t.getName());
	}

 }
}
