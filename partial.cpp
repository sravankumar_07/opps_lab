#include <iostream>
using namespace std;
class Job{
    public:
    virtual void work()=0;

    void salary(){
        cout << "30 thousand per month" << endl;
    }
};

class Mechanic : public Job {
    public:
    void work(){
        cout << "Repairs Car's and Bike's" << endl;
    }
};

class Shopkeeper : public Job{
    public:
    void work(){
        cout << "Selling good's and getting paid by people." << endl;
    }
};

int main(){
    Mechanic Mechanicher;
    Shopkeeper shopkeeper;
    Mechanicher.work();
    Mechanicher.salary();
    shopkeeper.work();
    shopkeeper.salary();
}
