#include<iostream>
using namespace std;
class Student
{  public:
      int code;
      string collegeName;
      string fullName;
      double sem;
    Student()
    {
        collegeName="MVGR";
        code=33;
    }
    Student(Student &obj)
    {
        cout<<"Enter your name : ";
        cin>>fullName;
        cout<<"Enter your percentage : ";
        cin>>sem;
        code=obj.code;
        collegeName=obj.collegeName;
    }
    void displayDetails()
    {
        cout<<"\t...STUDENT_DETAILS...\t\n";
        cout<<"Name  :"<<fullName<<endl;
        cout<<"College name  :"<<collegeName<<endl;
        cout<<"Code   : "<<code<<endl;
        cout<<"Percentage "<<sem<<endl;
    }
    ~Student()
    {
        cout<<"constructor is destroyed"<<endl;
    }

};
int main()
{
    Student obj1;
    Student obj2=obj1;
    obj2.displayDetails();
}
