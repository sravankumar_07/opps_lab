#include <iostream>
using namespace std;

class Remainder{
private:
    int dividend ;
    int divisior;
    public:
    Remainder(int a = 0  ,int b = 0)
    {
        dividend = a;
        divisior = b;
    }
    Remainder operator % (Remainder const &obj)
    {
        Remainder rem;
        rem.dividend = dividend % obj.dividend;
        rem.divisior = divisior % obj.divisior;
        return rem;
    }
    void print()
    {
        cout << "Remainders : "<< dividend << " , " << divisior << endl;
    }
};
int main()
{
    Remainder d1(10,11) ,d2(3,4);
    Remainder d3 = d1 % d2 ;
    d3.print();
}

