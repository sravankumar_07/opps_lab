class Class1{int num1=5;}
class Class2 extends Class1
{
       int num2=20;
       void display()
       {
        System.out.println(" This is Simple Inheritance.");
        System.out.println(" The value is(num1*num2) : " + num1*num2);
       }
}

class Class3 extends Class2
{
   void display1()
   {
        System.out.println(" This is Multilevel Inheritance.");
        System.out.println(" The value is(num2-num1) : " +(num2-num1));
   }
}
interface Class4{int num3=30;}
interface Class8{int num4=100;}

class Class5 implements Class8,Class4
 {
    void display2()
    {
        System.out.println(" This is Multiple Inheritance.");
        System.out.println(" The value is(num4+num3) : " + (num4+num3));
   }
}

class Class6 extends Class3
{
  
   void display3()
   {
    System.out.println(" This is Hybrid Inheritance.");
    System.out.println(" The value of (20^5) : " + num2*num1);
   }
}

class Class7 extends Class1
{
   
   void display4()
   {
           System.out.println(" This is Hirerachical Inheritance.");
           System.out.println(" The value is : " + num1);
   }
}
public  class TypesOfInheritance
{
   public static void main(String args[])
   {
       Class2 obj1=new Class2();
       obj1.display();
       Class3 obj2=new Class3();
       obj2.display1();
       Class5 obj3=new Class5();
       obj3.display2();
       Class6 obj4=new Class6();
       obj4.display3();
       Class7 obj5=new Class7();
       obj5.display4();
   }
}
