class BoxingUnBoxing {
    public void boxingUnBoxing() {
        //Boxing
        int counter = 30;
        Integer boxedCounter = counter;
        System.out.println("Boxed Counter: " + boxedCounter);

        //UnBoxing
        int unboxedCounter = boxedCounter;
        System.out.println("Unboxed Counter: " + unboxedCounter);
    }
    public static void main(String[] args) {
        BoxingUnBoxing obj = new BoxingUnBoxing();
        obj.boxingUnBoxing();
    }
}
