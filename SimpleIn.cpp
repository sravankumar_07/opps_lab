#include<iostream>
using namespace std;
class Inheritance{
    public:
    int num1 = 1;
    void parent()
    {
        cout << "I am the superclass.\n"; 
    }
};
class Simple : public Inheritance{
    public:
        int num2 = 18;
    void child(){
        
        cout << "I am child class inherited from the super class.\n";
        cout << "sum : " << num1+num2 << endl;
        cout << "This is simple inheritance.\n"; 
    }
};
int main(){
    Simple obj;
    obj.parent();
    obj.child();
}