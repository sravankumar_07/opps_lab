import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
public class FileInputStreamExample {
   public static void main(String args[]) throws IOException {
      //Creating a File object
      File file = new File("a.txt");
      //Creating a FileInputStream object
      FileInputStream inputStream = new FileInputStream(file);
      //Creating a byte array
      byte bytes[] = new byte[(int) file.length()];
      //Reading data into the byte array
      int numOfBytes = inputStream.read(bytes);
      FileOutputStream  outputStream = new FileOutputStream("b.txt");
      outputStream.write(bytes);
      outputStream.close();
      System.out.println("Data copied successfully...");

   }
}
