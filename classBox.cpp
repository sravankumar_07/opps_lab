#include<iostream>
using namespace std;
class Box{
    float length, width ,height; 
public:
    void welcomeMessage();
    float boxArea(float length,float width ,float height) // member function
    {      
            float area = 2*(length*height + length*width + width*height);
            return area;
    }
    float boxVolume(float length,float width ,float height); 
    friend void displayBoxDimensions(Box);
    void setFun( float l,float w ,float h);
};
void Box::setFun(float l,float w ,float h) {
   length = l;
   width = w;
   height = h;
}
void inline Box :: welcomeMessage()
    {
        cout<< "\t\t...Welcome to my program...\t"<<endl;
    }
void displayBoxDimensions(Box o) //friendfunction
{
    cout<<"Length is : " << o.length<< endl;
    cout<<"Width is : " << o.width << endl;
    cout<<"Height is : " << o.height << endl;
}
float Box :: boxVolume(float length,float width ,float height) // memberfunction outside the class
{                                                               // : is a scope resolution operator
 float volume = length * width * height ;
 return volume;
}
int main()
{
    Box obj;
    obj.welcomeMessage();
    float l,w,h;
    cout << "Enter the length,width & height of Box : ";
    cin >> l >> w >> h; 
    obj.setFun(l,w,h);
    if (l>0 && w>0 && h > 0)
    {
    displayBoxDimensions(obj);
    cout << "The Area of the box is : " << obj.boxArea(l,w,h) << endl;
    cout << "The Volume of the box is : " << obj.boxVolume(l,w,h) << endl;
    }
    else
    {
        cout << "Invalid!!!(Dimensions cannot be negetive)" << endl;
    }
}
