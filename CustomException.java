class MyException extends Exception {
}

class CustomExceptions{
	public static void main(String args[])
	{
		try {
			throw new MyException();
		}
		catch (MyException ex) {
			System.out.println("Caught");
			System.out.println(ex.getMessage());
		}
        
        finally
        {
            System.out.println("finally block");
        }
	}
}
