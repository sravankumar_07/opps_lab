#include<iostream>
using namespace std;
class Inheritance
{
    private:
        int a=10;
    protected:
        int b =100;
    public:
        int c = 1000;

        int PVT(){
        return a;
       }
}; 
class Private : private Inheritance{
    public:
        int getpub(){
            return c;
        }
        int getpro(){
            return b;
        }
};
class Protected : protected Inheritance{
    public:
        int getpro(){
                return b;
        }
        int getpub(){
            return c;
        }
};
class Public : public Inheritance{
  public:
   int getpro()
    {
        return b;
    }
    
};

int main(){
    Private obj;
    cout << "\t\t..Private Inheritance.." << endl;
    cout << " private : These are not ACCESSIBLE." << endl;
    cout << " PUBLIC : "<<obj.getpub() << endl;
    cout << " Protected : "<<obj.getpro() << endl;
    Protected obj1;
    cout << "\t\t..Protected Inheritance.." << endl;
    cout << " private : These are not ACCESSIBLE."<< endl;
    cout << " PUBLIC : "<<obj1.getpub() << endl;
    cout << " Protected : "<<obj1.getpro() << endl;
    Public obj2;
    cout << "\t\t..Public Inheritance.." << endl;
    cout << " private : "<< obj2.PVT()<< endl;
    cout << " PUBLIC : "<<obj2.c << endl;
    cout << " Protected : "<<obj2.getpro() << endl;
}