#include<iostream>
using namespace std;
class Method_Overloading{
    public:
    void add(int a,int b)
    {
        cout << "Sum(int) : " << a+b << endl;
    }
    void add(double a,double b)
    {
        cout << "Sum(flaot) : " << a+b << endl;
    }
    void add(string a,string b)
    {
        cout << "Concatenated String : " << a+b << endl;
    }
};
int main(){
    Method_Overloading obj;
    obj.add(2,34);
    obj.add(1.4,1.3);
    obj.add("Sravan ","Kumar");
    return 0;
}