#include <iostream>
using namespace std;

template <typename T>
T add(T num1, T num2) {
    return (num1 + num2);
}

int main() {
    int result1,a,b;
    double result2,a1,b1;
    string result3,s1,s2;
   
    cout << "Enter two integer numbers : ";
    cin >> a >> b;
    result1 = add<int>(a, b);
    cout << a <<" + " << b << " = " << result1 << endl;
    
    cout << "Enter two float point numbers : ";
    cin >> a1 >> b1;
    result2 = add<double>(a1, b1);
    cout << a1 <<" + " << b1 << " = " << result2 << endl;

    cout << "Enter two strings  : ";
    cin >> s1 >> s2;
    result3 = add<string>(s1, s2);
    cout << s1 <<" + " << s2 << " = " << result3 << endl;    

    return 0;
}