public class Function_Overloading {
   void add(int a,int b)
   {
    System.out.println("Sum(int) : " + (a+b));
   } 
   void add(double a, double b)
   {
    System.out.println("Sum(double) : " + (a+b));
   }
   void add(int a, int b,int c)
   {
    System.out.println("Sum(3 numbers) : " + (a+b+c));
   }
   void add(String a ,String b)
   {
    System.out.println("Cocatenated String  : " + (a+b));
   }
   public static void main(String[] args) {
    Function_Overloading  obj = new Function_Overloading();
    obj.add(2,45);
    obj.add(1.2,4.5);
    obj.add(2,45,7);
    obj.add("Sravan ", "Kumar");
   }
}
