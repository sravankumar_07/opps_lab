#include <iostream>
#include <iomanip>
using namespace std;
    int main()
    {
	    cout<< "Line 1" << endl << "Line 2" << endl << "Line 3" << endl;
	    cout<< "Line 1" << ends << "Line 2" << ends << "Line 3" << endl;
	    cout<<"USING setw() ..............\n";
	    cout<< setw(10) <<11<<"\n";
	    cout<< setw(10) <<2222<<"\n";
          cout<<"USING setw() & setfill()...\n";
	    cout<< setfill('-')<< setw(10) <<11<<"\n";
	    cout<< setfill('*')<< setw(10) <<2222<<"\n";
	    cout<<"USING fixed .......................\n";
	    cout.setf(ios::floatfield,ios::fixed);
	    cout<< setprecision(5)<<1234.537<< endl;
    }
