class Parent {
    double height = 5.6;
    void parent()
    {
        System.out.println("\t\t...I am the Parent Class...");
    }
}
class Child extends Parent{
    double weight = 56.7;
    void child()
    {
        System.out.println("\t\t...I am the Base Class...");

    }
}
public class SimpleIn{
    public static void main(String args[]) {
        Child obj = new Child();
        obj.parent();
        obj.child();
        System.out.println("The height of the child : " + obj.height);
        System.out.println("The weight of the child : " + obj.weight); 
    }
}