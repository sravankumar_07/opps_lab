#include<iostream>
using namespace std;
class Inher{};
class Mul{
    public:
    int num3;
    Mul()
    {
        cout << "Enter a3 value : " ;
        cin >> num3;
    }
};
class Inheritance{
    public:
    int num1;
    Inheritance()
    {
        cout << "Enter a1 value : " ;
        cin >> num1;
    }
};
class Simple : public Inheritance{      //simple inheritance
    public:
    int num2;
    Simple(){
        
        cout << "Enter a2 value : " ;
        cin >> num2;
    }
};
class Multilevel :  public Simple{          //Multilevel inheritance
    public:
    Multilevel()
    {
        cout << "Sum is : " << num2 + num1 << endl;
    }
    
};
class Multiple : public Multilevel,public Simple{      //multiple inheritance
    public:
    int num;
};
class Hierrchical :public Inher{     //hierarchical inheritance
    public:
    int num4;
    Hierrchical(){
        cout << "Enter a4 value : " ;
        cin >> num4;        
    } 
  
};
class Hybrid :  public Hierrchical, public Mul{      //hybrid inheritance
    public:
      Hybrid(){
        cout << "Sum :" << num3 +num4 << endl;
    }
   
};
int main()
{
    Multilevel obj1;
    Hybrid obj;
    return 0;
}