class ENE{
    void koushik()
    {
        System.out.println("\tShe paid noo...");
    }
    void vivek(){
        System.out.println("\tPsycho vivek");
    }
}
class ENE2 extends ENE{
    void koushik(){
        System.out.println("\tActor avutha...");
    }
    void vivek(){
        System.out.println("\tGoa podham");
    }
}
public class Method_Overriding{
    public static void main(String[] args) {
        ENE obj2 = new ENE();
        ENE2 obj = new ENE2();
        obj2.koushik();
        obj2.vivek();
        System.out.println("\t----After overriding-----");
        obj.koushik();
        obj.vivek();
    }
}
