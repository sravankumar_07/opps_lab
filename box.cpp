#include<iostream>
using namespace std;
#ifndef boxArea
    #include "boxArea.h"
#endif
#ifndef boxVolume
    #include "boxVolume.h"
#endif
int main()
{
    float l,w,h;
    cout << "Enter the l,w,h : " << endl;
    cin >> l >> w >> h ;
    if(l>0 &&  h > 0 && w >0)
    {
    cout << "The area of the Box is : " << boxArea(l,w,h) << endl;
    cout << "The volume of the Box is : " << boxVolume(l,w,h) << endl; 
    }
    else{
        cout << "Invalid dimensions cannot be negative." << endl;
    } 
     return 0;
