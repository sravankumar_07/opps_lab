class A{
     void add(int a,int b)
    {
     System.out.println("Sum(int) : " + (a+b));
    } 
}
class B extends A{
    void add(double a, double b)
    {
     System.out.println("Sum(double) : " + (a+b));
    }
}
class C extends B{
    void add(int a, int b,int c)
    {
     System.out.println("Sum(3 numbers) : " + (a+b+c));
    }
}
class D extends C{
    void add(String a ,String b)
    {
     System.out.println("Cocatenated String  : " + (a+b));
    }
}
class Function_Overloading_I {
    public static void main(String[] args) {
     D  obj = new D();
     obj.add(2,45);
     obj.add(1.2,4.5);
     obj.add(2,45,7);
     obj.add("Sravan ", "Kumar");
    }
 }
