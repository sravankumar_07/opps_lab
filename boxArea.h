float boxArea(float length,float width ,float height)
{
    float area = 2*(length*height + length*width + width*height);
    return area;
}