class Inheritance
{
    private int a=10;
    protected int b =100;
    public int c = 1000;

        int PVT(){
        return a;
       }
} 
class GetValues extends Inheritance{

        int getpub(){
            return c;
        }
        int getpro(){
            return b;
        }
}

public class PPPInheritance{
    public static void main(String[] args) {
    GetValues obj = new GetValues();
    System.out.println(" Private : " + obj.PVT());
    System.out.println( " Public : " + obj.getpub()) ;
    System.out.println( " Protected : " + obj.getpro());
    }
}
