import java.util.*;
public class Operation {
    public static void main(String[] args) {
        System.out.println("Enter any two integers : ");
        Scanner x = new Scanner(System.in);
        int p1 = x.nextInt();
        int p2 = x.nextInt();
        System.out.println("Choose the below operations");
        System.out.println("1.Addition");
        System.out.println("2.Subtraction");
        System.out.println("3.Multiplication");
        System.out.println("4.Division");
        System.out.println("5.Modulo");
        int op = x.nextInt();
        if(op==1)
        {
            System.out.println("Addition is " + (p1+p2));
        }
        else if(op==2){
            System.out.println("Subtraction is " + (p1-p2));
        }
        else if(op==3){
            System.out.println("Multiplication is " + (p1*p2));
        }
        else if(op==4){
            System.out.println("Division is " + (p1/p2));
        }
        else if(op==5){
            System.out.println("Modulo is " + (p1%p2));
        }
        else{
            System.out.println("Invalid input.");
        }
        x.close();
    }
    
}
