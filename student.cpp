#include <iostream>
using namespace std;
int isname(string name);
class Student{
  public:
    string fullName;
    int rollNum;
    double semPercentage;
    string collegname;
    int collegeCod;
    Student()
    {
        cout<<"....Constructed created...."<<endl;
    }
    Student(Student &obj)
    {
         cout << "Enter your name : ";
         cin >> fullName;
         if(!(isname(fullName)))
         {
            cout << "Entered a worng name." << endl;
            exit(0);
         }
         cout << "Roll no : ";
         cin >> rollNum;
         cout << "sem% : ";
         cin >> semPercentage;
         cout << "College name : ";
         cin >> collegname;
         if(!(isname(collegname)))
         {
            cout << "Entered a worng college name." << endl;
            exit(0);
         }
         cout << "College code : ";
         cin >> collegeCod;
    }
    void displaydetails()
    {
        cout << "\tHere are the details of mr/ms " << fullName << endl;
        cout <<"\t"<< fullName << "'s roll number is  : " << rollNum<<endl;
        cout << "\t"<<fullName << "'s sem % is : "<<semPercentage<<endl;
        cout <<"\t"<< fullName << " is studying in the college : " << collegname << endl;
        cout << "\tThe "<< collegname << " code is : " << collegeCod << endl;
    }
    ~Student()
    {
        cout << "....Constructed destroyed....."<<endl;
    }
};
int isname(string name)
{
    int x=0;
    for(int i =0; i<name.length();i++)
    {
        if(isdigit(name[i]) || !(isalpha(name[i])))
          {
              x=0;
              break;
          }
          else{x=1;}
        }
        if(x==0)
        {
            return 0;
        }
        else{
            return 1;
        }
}
int main() {
    Student obj1;
    Student obj2 = obj1;
    obj2.displaydetails();
    return 0;
}
